/*
	GNU LMLcSync
	Powered by www.leonardo.labolida.com
	
	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
	
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.
	
	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <dirent.h>
#include <stdio.h>
#include <sys/stat.h>
#include <cstdio>
#include <time.h>
#include <utime.h>
#include <string.h> 
#include <stdio.h>

/* Command-line arguments */
bool _verbose = false;
bool _keep = false;
char _format = false;


/* COPYFILE:  char * pathfilename1   copy_into-->  char * pathfilename2 */
void copy_file( char * pathfilename1 , char * pathfilename2 ) { 
	if (_verbose) printf("\n[COPY-FILE] %s %s" , pathfilename1 , pathfilename2 );
	char buf[BUFSIZ];
    size_t size;
    FILE* source = fopen( pathfilename1 ,"rb");
    FILE* dest   = fopen( pathfilename2 , "wb");
    while (size = fread(buf, 1, BUFSIZ, source)) {
        fwrite(buf, 1, size, dest);
    }
    fclose(source);
    fclose(dest);
}


/*
CORE 
	List files from directories: path1 , path2
	if path1_file_date > path2_file_date
	   if CheckIf fileSize>0 then
	   		then synchronize one way - left to right
*/
void core ( const char *path1  , const char *path2 ) {
	
	DIR * pdir1 = opendir (path1);  //DirectoryPointer_1
	DIR * pdir2 = opendir (path2);  //DirectoryPointer_2
	
	struct dirent * dir1 = NULL; //struct4fieldsFileName_1
	struct dirent * dir2 = NULL; //struct4fieldsFileName_2
	
	struct stat status1;
	struct stat status2;
	
	printf("\n  Dir1 %s ",path1);
	printf("\n  Dir2 %s ",path2);
	
	// Check errores
	if (pdir1 == NULL)	{ 
		printf ("\nERROR! Dir1 could not be initialised correctly with %s " , path1);
		return;
	} 
	if (pdir2 == NULL)	{ 
		printf ("\nERROR! Dir2 could not be initialised correctly with %s " , path2);
		return;
	} 
	
	while (dir1 = readdir (pdir1))	{
		if (dir1 == NULL) { 
			printf ("\nERROR! Dir1 could not be initialised correctly");
			return;
		}
		if (strcmp (dir1->d_name, "..") != 0  &&  strcmp (dir1->d_name, ".") != 0)  {	
			char fullpath1 [1024];
			char fullpath2 [1024];
			sprintf(fullpath1,"%s%s", path1, dir1->d_name );
			sprintf(fullpath2,"%s%s", path2, dir1->d_name );
			
			stat( fullpath1 , &status1 );                                       // FileStatus(filename)
			if (status1.st_mode & S_IFDIR ) {
	            // Append concat a slash at the end of the path
				if (_format=='p') {
		            strcat(fullpath1,"/");  // Posix-Unix
		            strcat(fullpath2,"/");  // Posix-Unix
	        	}else{
					strcat(fullpath1,"\\"); // MS-Win
	            	strcat(fullpath2,"\\"); // MS-Win
				}	
	            core( fullpath1 , fullpath2 );                                  // Recursive  ( CALL ME BACK )
			}else{
				if (_verbose) printf("\n  TimeCreat %i "  , status1.st_ctime);
				if (_verbose) printf(" timeModiff %i "  , status1.st_mtime);
				if (_verbose) printf(" %i bytes"   , status1.st_size);
				//if (_verbose) printf("\n[FILE1] %s" , dir1->d_name );
				if (_verbose) printf(" [FILE1] %s" , fullpath1 );

				stat(fullpath2, &status2); 
				
				if (_verbose) printf("\n  TimeCreat %i "  , status2.st_ctime);
				if (_verbose) printf(" timeModiff %i "  , status2.st_mtime);
				if (_verbose) printf(" %i bytes"   , status2.st_size);
				//if (_verbose) printf("\n[FILE2] %s" , dir1->d_name );
				if (_verbose) printf(" [FILE2] %s" , fullpath2 );

				/* 
				status1.st_atime > status2.st_atime ||
				status1.st_ctime > status2.st_ctime ||
				status1.st_mtime > status2.st_mtime
				We're gonna validate just the modiffied dates!
				*/
				if ( status1.st_mtime > status2.st_mtime ) {
					if (_verbose) printf("\n COPY %s %s " , fullpath1 , fullpath2 ); 
					if (_keep) {
						copy_file( fullpath1 , fullpath2 );
					 	struct utimbuf newtime;
					 	newtime.actime   = status1.st_atime;
					 	newtime.modtime  = status1.st_mtime;
					 	utime(fullpath2, &newtime);
						 }
				 }else{
				 	if (_verbose) printf("[OK]");
				 }
			}
		}
	}
	closedir (pdir1);
	closedir (pdir2);
}

/* Print GNU License*/
void license(){
	printf("\n  This program is free software: you can redistribute it and/or modify");
	printf("\n  it under the terms of the GNU General Public License as published by");
	printf("\n  the Free Software Foundation, either version 3 of the License, or");
	printf("\n  (at your option) any later version.");
	printf("\n  ");
	printf("\n  This program is distributed in the hope that it will be useful,");
	printf("\n  but WITHOUT ANY WARRANTY; without even the implied warranty of");
	printf("\n  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the");
	printf("\n  GNU General Public License for more details.");
	printf("\n  ");
	printf("\n  You should have received a copy of the GNU General Public License");
	printf("\n  along with this program.  If not, see <http://www.gnu.org/licenses/>.");
	printf("\n  This program was developed by leonardo.labolida.com");
	printf("\n  ");
}


/* MAIN */
int main ( int argc, char *argv[] ) {
	printf("  GNU LMLcSync version 1.0 2015");
	if ( argc<2) {
		license();
		printf("\n  Usage:");
		printf("\n  ");
		printf("\n  Parameters:  path-origin path-destiny direction verbose keep format");
		printf("\n  1: path-origin: The source directory");
		printf("\n  2: path-destiny: The destiny directory");
		printf("\n  3: direction: 1=LeftToRight, 2=BothDirections");
		printf("\n  4: verbose: v=show information detail");
		printf("\n  5: keep: k=it will keep files the way that they are. No changes will be made.");
		printf("\n  6: Directory format: p=Posix(Linux/Unix) or w=Windows");
		printf("\n  ");
		printf("\n  Examples:");
		printf("\n    ./lmlcsync /user/dir/ /usb/hdz/ 1 - - p ");
		printf("\n    ./lmlcsync /user/dir/ /usb/hdz/ 2 - k p");
		printf("\n    lmlcsync.exe c:\\win\\dir\\ f:\\win\\dir\\ 2 v k w");
		printf("\n   Try it: ./lmlcsync /user/dir/ /usb/hdz/ 2 v k p >log");
	}else{
		if (argv[4][0]=='v') _verbose=true;  else _verbose=false;
		if (argv[5][0]=='k') _keep=false;    else _keep=true;
		if (argv[6][0]=='p') _format='p';    else _format='w';
		
		core ( argv[1] , argv[2] );              // DIRECTION Left2Right
		
		if ( argv[3][0]=='2') {                  // both DIRECTIONS
			core ( argv[2] , argv[1] );
		}
	}
	return 0;
}

